/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
'use strict';
//var medico = require('../modelos/medico');
//var cuenta = require('../modelos/cuenta');
class Test {

    separador(req, res) {


    }

    formulaGeneral(req, res) {
        var a = req.params.a;
        var b = req.params.b;
        var c = req.params.c;
        a = parseInt(a);
        b = parseInt(b);
        c = parseInt(c);
        console.log(a + " " + b + " " + c)
        var x = ((-b) + (Math.sqrt((Math.pow(b, 2) - (4 * a * c))))) / (2 * a);
        var x1 = ((-b) - (Math.sqrt((Math.pow(b, 2) - (4 * a * c))))) / (2 * a);
        console.log(x + " " + x1 + " ");
        res.send("Valor x1 = " + x + " Valor X2 = " + x1);

    }

    aceleracion(req, res) {
        var vi = req.params.vi;
        var vf = req.params.vf;
        var t = req.params.t;
        vi = parseInt(vi);
        vf = parseInt(vf);
        t = parseInt(t);
        var aceleracion = (vf - vi) / t;
        console.log(aceleracion);
        res.send("La aceleracion es =" + aceleracion + "");
    }

    trianguloPascal(req, res) {
        var n = req.params.n; //número de filas  
        var texto = '';
        //creamos las matrices A(n) y B(n)  
        var A = new Array(n); //vector antiguo, es el previo  
        var B = new Array(n); //vector nuevo  
        var centro = '<center>';

        //convertimos en cero todos los elementos del vector A(n)  
        for (var k = 0; k <= n; k++) {
            A[k] = 0;
        }

        //alimentamos la matriz  
        A[1] = 1; //El primer 1 del vértice del triángulo  
        texto = A[1] + '<br>'; //imprimimos el vértice  
        for (var i = 2; i <= n; i++) { //i nos da la fila  
            for (var j = 1; j <= i; j++) { //j nos da la columna  
                B[j] = A[j - 1] + A[j]; //los elementos de B se forman sumando 2 elementos de A  

                texto += B[j] + " "; //imprimimos un elemento  
            }
            for (j = 1; j <= i; j++) {
                A[j] = B[j]; //el vector B se convierte en antiguo  

            }

            texto += "<br>"; //salto para una nueva fila  
        }
        //document.getElementById("resultado").innerHTML = texto;
        centro += texto + '</center>';
        res.send(texto);
    }

    genera(req, res) {
        var a = req.body.a;
        var b = req.body.b;
        var c = {};
        for (var i = 1; i <= b; i++) {
            c[i - 1] = { "desc": a + "*" + i, "resp1": (a * i) };
        }
        res.render('index1', { title: 'Hello word', sesion: true, fragmento: 'fragmentos/resultado', resp: c, dato: b });
        //res.send(c);
    }
}
module.exports = Test;