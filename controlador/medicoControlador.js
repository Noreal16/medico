'use strict';
var medico = require('../modelos/medico');
var cuenta = require('../modelos/cuenta');
class medicoControlador {

    guardar(req, res) {
        var datos = {
            cedula: req.body.cedula,
            apellidos: req.body.apellido,
            nombres: req.body.nombre,
            fecha_nac: req.body.fecha_nac,
            edad: req.body.edad,
            nro_registro: req.body.nro_registro,
            especialidad: req.body.especialidad
        };
        var datoC = {
            correo: req.body.correo,
            clave: req.body.clave
        };
        var Medico = new medico(datos);
        var Cuenta = new cuenta(datoC);
        Medico.cuenta = Cuenta;
        Medico.saveAll({ cuenta: true }).then(function(resutado) {
            req.flash('success', 'Se ha registrado correctamente!');
            res.redirect('/');
        }).error(function(error) {
            res.send(error);
        });
    }

}
module.exports = medicoControlador;