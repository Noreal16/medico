'use strict';
var cuentaC = require('../modelos/cuenta');

class cuentaControl {
    /**
     * clase ue permite iniciar secion y cerrar sesion
     * @param {type} req 
     * @param {type} res 
     */
    iniciarSesion(req, res) {
        //var cuenta = new cuentaC();
        cuentaC.getJoin({ medico: true }).filter({ correo: req.body.correo }).run().then(function(resultado) {
            if (resultado.length > 0) {
                var cuenta = resultado[0];
                if (cuenta.clave === req.body.clave) {
                    req.session.cuenta = {
                        external: cuenta.medico.external_id,
                        usuario: cuenta.medico.apellidos + " " + cuenta.medico.nombres
                    };
                    res.redirect('/')
                        //res.send(cuenta);

                } else {
                    req.flash('error', 'Sus credenciales son incorrectas!');
                    res.redirect('/');
                }

            } else {
                req.flash('error', 'Sus credenciales son incorrectas!');
                res.redirect('/');
            }

        }).error(function(error) {

        });
    }
    cerrae_sesion(req, res) {
        req.session.destroy();
        res.redirect('/');
    }
}
module.exports = cuentaControl;