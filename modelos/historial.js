var thinky = require('../config/thinky_init');
var type = thinky.type;
var r = thinky.r;
var Historial = thinky.createModel("Historial", {
    id: type.string(),
    contacto: type.string(),
    enfermedades: type.string(),
    enfermedades_hereditarias: type.string(),
    external_id: type.string().default(r.uuid()),
    habitos: type.string(),
    createAt: type.date().default(r.now()),
    id_paciente: type.string()
});
module.exports = Historial;
var Paciente = require("./paciente");
Historial.belongsTo(Paciente, "paciente", "id", "id_paciente");
var HistorialClinico = require("./historiaClinica");
Historial.hasMany(HistorialClinico, "historialClinico", "id", "id_historial");