var express = require('express');
var router = express.Router();
var utiles = require('../controlador/utiles');
var test1 = require('../controlador/Test');
var test = new test1();
var medicoC = require('../controlador/medicoControlador');
var medC = new medicoC();
var cuentaControl = require('../controlador/cuentaControl');
var control = new cuentaControl();
/**
mildware es como cortaFuegis
 */
function verificar_inicio(req) {
    return (req.session !== undefined && req.session.cuenta !== undefined);
}
/**
 * configuracion para verificar si se inicio sesion
 * @param {*} req 
 * @param {*} res para redireccionar al inicio en caso de error deque no se inicio sesion
 * @param {*} next 
 */
var auth = function(req, res, next) {
        if (verificar_inicio(req)) {
            next();
        } else {
            req.flash('error', 'Debes iniciar sesion');
            res.redirect('/');
        }
    }
    /**
     * validar al usuario que existe en la cuenta 
     */
router.get('/', function(req, res, next) {
    if (req.session !== undefined && req.session.cuenta !== undefined) {
        res.render('index1', {
            title: "Principal",
            fragmento: 'fragmentos/principal',
            sesion: true,
            usuario: req.session.cuenta.usuario
        });

    } else {
        res.render('index1', {
            title: 'Medico',
            msg: {
                error: req.flash('error'),
                ok: req.flash('success')
            }

        });
    }

});
/**
 * registro medico
 */
router.get('/registro', function(req, res, next) {
    res.render('index1', { title: 'Registro Medico', sesion: true, fragmento: 'fragmentos/registroMedico' });
});
/**Registro medico */
router.post('/registroMedico', medC.guardar);
/**Logion */
router.post('/inicio_sesion', control.iniciarSesion);
router.get('/cerrar_sesion', auth, control.cerrae_sesion);
/*Llama al fragmento paciente*/
router.get('/paciente', function(req, res, next) {
    res.render('index1', { title: 'Registro Medico', sesion: true, fragmento: 'fragmentos/paciente' });
});

/******************************************************************************************************* */
/* GET home page. */
router.get('/admin', function(req, res, next) {
    res.render('index1', { title: 'Registro Medico', sesion: true, fragmento: 'fragmentos/fragmento' });
});


/*Metodo para presentar el nombre */
router.get('/hola', function(req, res, next) {
    res.send('santiago');
});

/*Metodo para hacer una suma */
router.get('/sumas/:a/:b', function(req, res, next) {
    var a = req.params.a * 1;
    var b = req.params.b * 1;
    var c = a + b;
    res.send(c + "");
});
/*Metodo para sacar el fibonacci*/
router.get('/fib/:n', function(req, res, next) {
    var a = 1;
    var b = 0;
    var datos = '';
    var n = req.params.n * 1;
    for (var i = 0; i < n; i++) {
        var fib = parseInt(a + b);
        a = b;
        b = fib;
        datos += (fib + ",");
    }
    res.send(datos);
});

router.get('/suma/:a/:b', utiles.sumar);
router.get('/resta/:a/:b', utiles.resta);

//llamar practica 5
router.get('/formulaGeneral/:a/:b/:c', test.formulaGeneral);
router.get('/aceleracion/:vi/:vf/:t', test.aceleracion);
router.get('/trianguloPascal/:n', test.trianguloPascal);
router.post('/admin/generar', test.genera);
module.exports = router;
//inyeccion  node por debajo ya pone la variable req o ress